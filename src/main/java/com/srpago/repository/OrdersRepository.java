package com.srpago.repository;

import com.srpago.model.GasOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrdersRepository extends JpaRepository<GasOrder, Long> {
}
