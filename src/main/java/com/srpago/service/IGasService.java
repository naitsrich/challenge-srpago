package com.srpago.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.srpago.model.GasOrder;
import com.srpago.model.dto.in.GasolineDto;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public interface IGasService {
    public GasOrder createAndPersistGasOrder(GasOrder gasOrder);
    public GasOrder createOrder (GasolineDto g, GasOrder gasOrder);
    public Optional<GasOrder> findById(Long id);
}
