package com.srpago.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.srpago.model.Client;
import com.srpago.repository.ClientsRepository;
import com.srpago.repository.OrdersRepository;
import com.srpago.client.IGasolineClient;
import com.srpago.client.IPaymentClient;
import com.srpago.model.GasOrder;
import com.srpago.model.dto.in.GasolineDto;
import com.srpago.service.IGasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Optional;

@Service
public class GasService implements IGasService {

    @Autowired
    private IGasolineClient gasolineClient;

    @Autowired
    private OrdersRepository ordersRepository;

    @Autowired
    private ClientsRepository clientsRepository;

    @Autowired
    private IPaymentClient paymentClient;

    @Override
    @Transactional
    public GasOrder createAndPersistGasOrder(GasOrder gasOrder) {
        GasolineDto a  = gasolineClient.findById(gasOrder.getGasStationId());
        gasOrder = this.createOrder(a, gasOrder);
        return ordersRepository.save(gasOrder);
    }


    @Override
    public GasOrder createOrder (GasolineDto g, GasOrder gasOrder) {
        String paymentId = paymentClient.pay(gasOrder.getPayment());
        Optional<Client> client = clientsRepository.findByEmail(gasOrder.getClient().getEmail());
        client.ifPresentOrElse(c -> gasOrder.setClient(c), () -> { clientsRepository.save(gasOrder.getClient());});
        BigDecimal price = g.getGasolinePrice(gasOrder.getGasType());
        BigDecimal finalPrice = price.multiply(gasOrder.getAmount());
        gasOrder.setPaymentId(paymentId);
        gasOrder.setFinalPrice(finalPrice);
        gasOrder.setGasPrice(price);
        return gasOrder;
    }

    @Override
    public Optional<GasOrder> findById(Long id) {
        return ordersRepository.findById(id);
    }


}
