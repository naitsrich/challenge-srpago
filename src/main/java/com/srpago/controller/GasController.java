package com.srpago.controller;

import com.srpago.exception.EntityNotFoundException;
import com.srpago.model.GasOrder;
import com.srpago.model.Response;
import com.srpago.model.ResponseBuilder;
import com.srpago.model.dto.in.InfoRQ;
import com.srpago.service.IGasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("gas")
public class GasController {

    @Autowired
    IGasService gasService;

    /*
    Este endpoint no era necesario pero lo agregué para consultas
     */
    @GetMapping("/order/{id}")
    public GasOrder getAllOrders(@PathVariable Long id) {
        return gasService.findById(id).map(x -> x).orElseThrow(() -> new EntityNotFoundException("Order Not Found", "Order not found: " + id));
    }

    @PostMapping(path = "/order")
    public ResponseEntity<Response> generateOrder(@RequestBody @Valid InfoRQ gas) {
            gasService.createAndPersistGasOrder(GasOrder.from(gas));
            return new ResponseEntity<Response>(
                    new ResponseBuilder().withSuccess().withMessage("Información correcta").build(),
                    HttpStatus.OK);
    }
}
