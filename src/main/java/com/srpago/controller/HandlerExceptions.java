package com.srpago.controller;

import com.srpago.exception.EntityNotFoundException;
import com.srpago.model.Response;
import com.srpago.model.ResponseBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
public class HandlerExceptions {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Response> handleValidationApiExceptions(MethodArgumentNotValidException ex) {
        List<String> errors = ex.getBindingResult().getFieldErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.toList());
        return new ResponseEntity<Response>(
                new ResponseBuilder().withNoSucess().withErrorMessage("Validation Error").withMessage(errors.toString()).build(),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<Response> handleEntityNotFoundException(EntityNotFoundException ex) {
        return new ResponseEntity<Response>(
                new ResponseBuilder().withNoSucess().withErrorMessage(ex.getMessage()).withMessage(ex.getCustomMessage()).build(),
                HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<Response> handlerInvalidRequestException(HttpMessageNotReadableException ex) {
        return new ResponseEntity<Response>(
                new ResponseBuilder().withNoSucess().withErrorMessage("Invalid Request").withMessage(ex.getLocalizedMessage()).build(),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Response> handleNotExpectedExceptions(Exception ex) {
        ex.printStackTrace();
        return new ResponseEntity<Response>(
                new ResponseBuilder().withNoSucess().withErrorMessage("Internal Server Error").withMessage(ex.getLocalizedMessage()).build(),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
