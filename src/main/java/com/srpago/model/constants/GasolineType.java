package com.srpago.model.constants;

import java.util.Arrays;
import java.util.Optional;

public enum GasolineType {
    REGULAR(1),
    PREMIUM(2);

    private Integer type;

    GasolineType(Integer i) {
        this.type = i;
    }

    public Integer getNumber() {
        return type;
    }

    public static Optional<GasolineType> valueOf(int a) {
        return Arrays.stream(GasolineType.values()).filter(x -> x.getNumber().equals(a)).findFirst();
    }
}
