package com.srpago.model;

import com.srpago.model.constants.GasolineType;
import com.srpago.model.converters.GasTypeConverter;
import com.srpago.model.dto.in.InfoRQ;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Optional;

@Entity
@Table(name="orders")
public class GasOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "client_id")
    private Client client;

    @Transient
    private Payment payment;

    @Column(name="gas_type")
    @Convert(converter = GasTypeConverter.class)
    private GasolineType gasType;

    @Column(name="amount")
    private BigDecimal amount;

    @Column(name="gas_station_id")
    private String gasStationId;

    @Column(name="seller_name")
    private String sellerName;

    @Column(name="gas_price")
    private BigDecimal gasPrice ;

    @Column(name="final_price")
    private BigDecimal finalPrice;

    @Column(name="payment_id")
    private String paymentId;

    public GasOrder(Client client, Payment payment, GasolineType gasType, BigDecimal amount, String gasStationId, String sellerName) {
        this.client = client;
        this.payment = payment;
        this.gasType = gasType;
        this.amount = amount;
        this.gasStationId = gasStationId;
        this.sellerName = sellerName;
    }

    public GasOrder() {}


    public static GasOrder from(InfoRQ inforq) {
        Client client = new Client(inforq.getEmail(), inforq.getName(), inforq.getLastName());
        Payment payment = new Payment(inforq.getCardNumber(), inforq.getExpirationDateYear(), inforq.getExpirationDateMonth());

        Optional<GasolineType> a = GasolineType.valueOf(inforq.getGasType());

        if(!a.isPresent()) {
            throw new RuntimeException("Not Found");
        }
        return new GasOrder(client, payment, a.get(), new BigDecimal(inforq.getAmount()), inforq.getGasStation(), inforq.getSellerName());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public String getGasStationId() {
        return gasStationId;
    }

    public void setGasStationId(String gasStationId) {
        this.gasStationId = gasStationId;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getGasPrice() {
        return gasPrice;
    }

    public void setGasPrice(BigDecimal gasPrice) {
        this.gasPrice = gasPrice;
    }

    public BigDecimal getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(BigDecimal finalPrice) {
        this.finalPrice = finalPrice;
    }

    public GasolineType getGasType() {
        return gasType;
    }

    public void setGasType(GasolineType gasType) {
        this.gasType = gasType;
    }
}
