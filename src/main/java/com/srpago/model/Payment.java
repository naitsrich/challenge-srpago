package com.srpago.model;


public class Payment {

    private String cardNumber;

    private Integer expirationDateYear;

    private String expirationDateMonth;

    public Payment(String cardNumber, Integer expirationDateYear, String expirationDateMonth) {
        this.cardNumber = cardNumber;
        this.expirationDateYear = expirationDateYear;
        this.expirationDateMonth = expirationDateMonth;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Integer getExpirationDateYear() {
        return expirationDateYear;
    }

    public void setExpirationDateYear(Integer expirationDateYear) {
        this.expirationDateYear = expirationDateYear;
    }

    public String getExpirationDateMonth() {
        return expirationDateMonth;
    }

    public void setExpirationDateMonth(String expirationDateMonth) {
        this.expirationDateMonth = expirationDateMonth;
    }
}
