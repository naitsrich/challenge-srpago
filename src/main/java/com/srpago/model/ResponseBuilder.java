package com.srpago.model;

public class ResponseBuilder {
    private Response response = new Response();

    public ResponseBuilder withSuccess() {
        this.response.setSuccess(true);
        return this;
    }

    public ResponseBuilder withNoSucess() {
        this.response.setSuccess(false);
        return this;
    }

    public ResponseBuilder withMessage(String message) {
        this.response.setMessage(message);
        return this;
    }

    public ResponseBuilder withErrorMessage(String errorMessage) {
        this.response.setError(errorMessage);
        return this;
    }

    public Response build() {
        return this.response;
    }
}
