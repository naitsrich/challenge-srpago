package com.srpago.model.converters;

import com.srpago.model.constants.GasolineType;

import javax.persistence.AttributeConverter;

public class GasTypeConverter implements AttributeConverter<GasolineType, Integer> {

    @Override
    public Integer convertToDatabaseColumn(GasolineType g) {
        return g.getNumber();
    }

    @Override
    public GasolineType convertToEntityAttribute(Integer numberGasType) {
        return GasolineType.valueOf(numberGasType).get();
    }
}
