package com.srpago.model.dto.in;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.srpago.model.constants.GasolineType;

import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GasolineDto {
    private String _id; //id

    private BigDecimal regular; //regular

    private BigDecimal premium; //premium


    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public BigDecimal getRegular() {
        return regular;
    }

    public void setRegular(BigDecimal regular) {
        this.regular = regular;
    }

    public BigDecimal getPremium() {
        return premium;
    }

    public void setPremium(BigDecimal premium) {
        this.premium = premium;
    }

    public BigDecimal getGasolinePrice(GasolineType gasType) {
        if(gasType == GasolineType.REGULAR) {
            return this.getRegular();
        }
        if(gasType == GasolineType.PREMIUM) {
            return this.getPremium();
        }
        return null;
    }
}

/*
{
    "pagination": {
        "pageSize": 100,
        "page": 1,
        "total": 1
    },
    "results": [
        {
            "_id": "587fbd68edfe99480a072f14",
            "calle": "Av. Adolfo López Mateos No. 1604  Col. Melchor Ocampo  Juárez",
            "rfc": "DGA930823KD3",
            "date_insert": "2017-01-18T19:09:26.784Z",
            "regular": "15.71",
            "colonia": "",
            "numeropermiso": "PL/760/EXP/ES/2015",
            "fechaaplicacion": "",
            "permisoid": "2041",
            "longitude": "-106.4514",
            "latitude": "31.71947",
            "premium": "17.93",
            "razonsocial": "DÍAZ GAS  S.A. DE C.V.",
            "codigopostal": "32380",
            "dieasel": ""
        }
    ]
}
 */