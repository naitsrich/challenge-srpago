package com.srpago.model.dto.in;


import javax.validation.constraints.*;


public class InfoRQ{

    @Email(message = "Email must have a valid email format")
    @NotBlank(message = "Email must not be null")
    private String email;

    @NotBlank(message = "Name must not be blank")
    private String name;

    @NotBlank(message = "lastName must not be blank")
    private String lastName;

    @NotBlank(message = "cardNumber must not be blank")
    @Pattern(regexp = "[0-9]{16}", message = "cardNumber must have 16 numbers")
    private String cardNumber;

    @NotNull(message = "expirationDateYear must not be null")
    @Min(value = 1990, message = "expirationDateYear must be 1990 or bigger")
    private Integer expirationDateYear;

    @NotBlank(message = "expirationDateMonth must not be blank")
    @Pattern(regexp = "[1-9]|1[0-2]", message = "expirationDateMonth must be a valid month from 1 to 12")
    private String expirationDateMonth;

    @NotNull(message = "gasType must not be null")
    @Min(value = 1, message = "gasType must be 1 for Regular or 2 for Premium")
    @Max(value = 2, message = "gasType must be 1 for Regular or 2 for Premium")
    private Integer gasType;

    @NotNull(message = "amount must not be null")
    @Min(value = 1, message = "Amount must be at least 1")
    private Double amount;

    @NotBlank(message = "gasStation must not be blank")
    private String gasStation;

    @NotBlank(message = "sellerName must not be blank")
    private String sellerName;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Integer getExpirationDateYear() {
        return expirationDateYear;
    }

    public void setExpirationDateYear(Integer expirationDateYear) {
        this.expirationDateYear = expirationDateYear;
    }

    public String getExpirationDateMonth() {
        return expirationDateMonth;
    }

    public void setExpirationDateMonth(String expirationDateMonth) {
        this.expirationDateMonth = expirationDateMonth;
    }

    public Integer getGasType() {
        return gasType;
    }

    public void setGasType(Integer gasType) {
        this.gasType = gasType;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getGasStation() {
        return gasStation;
    }

    public void setGasStation(String gasStation) {
        this.gasStation = gasStation;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }


}
