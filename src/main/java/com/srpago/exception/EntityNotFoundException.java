package com.srpago.exception;

public class EntityNotFoundException extends RuntimeException{
    private String message;

    public EntityNotFoundException(String errorMessage, String message) {
        super(errorMessage);
        this.message =message;
    }

    public String getCustomMessage() {
        return message;
    }
}
