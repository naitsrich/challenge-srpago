package com.srpago.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.srpago.model.dto.in.GasolineDto;

public interface IGasolineClient {
    GasolineDto findById(String id);
}
