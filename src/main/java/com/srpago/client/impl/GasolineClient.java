package com.srpago.client.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.srpago.client.IGasolineClient;
import com.srpago.exception.EntityNotFoundException;
import com.srpago.model.dto.in.GasolineDto;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class GasolineClient implements IGasolineClient {
    //private String urlGasoline = "http://localhost:9999/v1/precio.gasolina.publico";

    @Value("${gasoline.api}")
    private String gasolineApiUrl;

    @Autowired
    ObjectMapper objectMapper;

    @Override
    public GasolineDto findById(String id) {
        HttpResponse<JsonNode> a = Unirest.get(gasolineApiUrl)
                .queryString("_id", id)
                .asJson();

        a.ifFailure(Error.class, r -> {
            throw new EntityNotFoundException("Invalid GasStation", "Gas Station " + id + " not Found");
        });

        try {
            return objectMapper.readValue(a.getBody().getObject().getJSONArray("results").get(0).toString(), GasolineDto.class);
        }
        catch(Exception ex) {
            throw new EntityNotFoundException("Invalid GasStation", "Gas Station " + id + " not Found");
        }
    }
}
