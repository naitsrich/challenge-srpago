package com.srpago.client;

import com.srpago.model.Payment;

public interface IPaymentClient {
    String pay(Payment payment);
}

