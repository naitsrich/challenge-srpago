package com.srpago.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.srpago.model.dto.in.InfoRQ;
import com.srpago.test.BaseTest;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.servlet.ServletContext;
import java.util.Arrays;
import java.util.List;


public class GasControllerExceptionsTest extends BaseTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private ObjectMapper objectMapper;

    private MockMvc mockMvc;
    @BeforeEach
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    @Test
    public void givenWac_whenServletContext_thenItProvidesGreetController() {
        ServletContext servletContext = webApplicationContext.getServletContext();

        Assert.assertNotNull(servletContext);
        Assert.assertTrue(servletContext instanceof MockServletContext);
        Assert.assertNotNull(webApplicationContext.getBean("gasController"));
    }

    @Test
    public void shouldReturnBadRequestWhenCreatingOrderWithoutBody() throws Exception {
        final String expectedBody = "{\"success\":false,\"error\":\"Invalid Request\",\"message\":\"Required request body is missing: public org.springframework.http.ResponseEntity<com.srpago.model.Response> com.srpago.controller.GasController.generateOrder(com.srpago.model.dto.in.InfoRQ)\"}";
        MvcResult mvcResult = this.mockMvc.perform(post("/gas/order"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(expectedBody))
                .andReturn();
    }

    @Test
    public void shouldReturnValidationErrorDueToInvalidBody() throws Exception {
        final String expectedBody = "{\"success\":false,\"error\":\"Validation Error\",\"message\":\"[gasStation must not be blank, cardNumber must not be blank, expirationDateMonth must not be blank, Email must not be null, expirationDateYear must not be null, lastName must not be blank, amount must not be null, sellerName must not be blank, gasType must not be null, Name must not be blank]\"}";
        InfoRQ infoRQ = new InfoRQ();
        infoRQ.setEmail("");


        String body = objectMapper.writeValueAsString(infoRQ);

        MvcResult mvcResult = this.mockMvc.perform(
                post("/gas/order")
                .contentType(MediaType.APPLICATION_JSON).content(body)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andReturn();

        String content = mvcResult.getResponse().getContentAsString();

        List<String> errors = Arrays.asList("gasStation must not be blank", "cardNumber must not be blank", "expirationDateMonth must not be blank", "Email must not be null", "expirationDateYear must not be null", "lastName must not be blank", "amount must not be null", "sellerName must not be blank", "gasType must not be null", "Name must not be blank");

        errors.forEach(x -> Assert.assertTrue(content.contains(x)));
    }
}
