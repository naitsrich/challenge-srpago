package com.srpago.test.utils;

import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;

public class FileUtils {

    public static String readAsText(String path) throws IOException {
        File file = ResourceUtils.getFile("classpath:" + path);
        String content = new String(Files.readAllBytes(file.toPath()));
        return content;
    }
}
