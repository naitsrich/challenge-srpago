package com.srpago.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.srpago.model.Client;
import com.srpago.model.GasOrder;
import com.srpago.model.constants.GasolineType;
import com.srpago.model.dto.in.GasolineDto;
import com.srpago.model.dto.in.InfoRQ;
import com.srpago.repository.ClientsRepository;
import com.srpago.repository.OrdersRepository;
import com.srpago.service.IGasService;
import com.srpago.test.utils.FileUtils;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import java.io.IOException;
import java.math.RoundingMode;
import java.util.List;
import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;

public class GasServiceTest extends BaseTest {

    @AfterEach
    public void resetDataBase() {
        ordersRepository.deleteAllInBatch();
        clientsRepository.deleteAllInBatch();
    }

    @Autowired
    private IGasService gasService;

    @Autowired
    private ClientsRepository clientsRepository;

    @Autowired
    private OrdersRepository ordersRepository;

    @Test
    public void shouldCreateValidOrder() throws IOException {
        GasolineDto gasolineDto = new ObjectMapper().readValue(FileUtils.readAsText("mocks/api/responses/gasolineResponse.json"), GasolineDto.class);
        GasOrder gasOrder = GasOrder.from(new ObjectMapper().readValue(FileUtils.readAsText("mocks/api/responses/infoRQRequest.json"), InfoRQ.class));
        gasOrder = gasService.createOrder(gasolineDto, gasOrder);

        Assertions.assertEquals(gasOrder.getFinalPrice().setScale(2, RoundingMode.HALF_DOWN).toString(), "5514.21");
        Assertions.assertEquals(gasOrder.getGasPrice().setScale(2, RoundingMode.HALF_DOWN).toString(), "15.71");
        Assertions.assertEquals(gasOrder.getGasType(), GasolineType.REGULAR);
    }

    @Test
    public void shouldCreateAndPersistGasOrderAndClient() throws IOException {

        String id = "587fbd68edfe99480a072f15";

        //Se debería mockear el service mas que la respuesta ...
        wireMockRule.stubFor(WireMock.get("/v1/precio.gasolina.publico?_id=587fbd68edfe99480a072f15").willReturn(aResponse()
                .withHeader("Content-Type", "application/json")
                .withBody(FileUtils.readAsText("mocks/api/responses/gasolineApiByIdResponse.json"))));

        GasolineDto gasolineDto = new ObjectMapper().readValue(FileUtils.readAsText("mocks/api/responses/gasolineResponse.json"), GasolineDto.class);
        GasOrder gasOrder = GasOrder.from(new ObjectMapper().readValue(FileUtils.readAsText("mocks/api/responses/infoRQRequest.json"), InfoRQ.class));
        gasOrder = gasService.createAndPersistGasOrder(gasOrder);

        Assertions.assertEquals(gasOrder.getFinalPrice().setScale(2, RoundingMode.HALF_DOWN).toString(), "5514.21");
        Assertions.assertEquals(gasOrder.getGasPrice().setScale(2, RoundingMode.HALF_DOWN).toString(), "15.71");

        gasService.findById(gasOrder.getId()).ifPresentOrElse(x -> {
            Assertions.assertEquals(x.getFinalPrice().setScale(2, RoundingMode.HALF_DOWN).toString(), "5514.21");
            Assertions.assertEquals(x.getGasPrice().setScale(2, RoundingMode.HALF_DOWN).toString(), "15.71");
            Assertions.assertEquals(x.getGasType(), GasolineType.REGULAR);

        }, () -> {
            throw new RuntimeException("Order not found");
        });
    }

    @Test
    public void shouldThrowExceptionDueToInvalidGasStationId() throws IOException {

        String id = "lalala";

        wireMockRule.stubFor(WireMock.get("/v1/precio.gasolina.publico?_id=lalala").willReturn(aResponse()
                .withHeader("Content-Type", "application/json")
                .withStatus(500)));

        final GasOrder gasOrder = GasOrder.from(new ObjectMapper().readValue(FileUtils.readAsText("mocks/api/responses/infoRQRequest.json"), InfoRQ.class));

        Assertions.assertThrows(RuntimeException.class, () -> {
            gasService.createAndPersistGasOrder(gasOrder);
        });

    }

    @Test
    public void shouldCreateAndPersistOrderForExistingClient() throws IOException {

        String id = "587fbd68edfe99480a072f15";

        Client client = new Client("info@srpago.com","Juan","Perez");
        client = clientsRepository.save(client);

        Assertions.assertEquals("info@srpago.com", client.getEmail());

        wireMockRule.stubFor(WireMock.get("/v1/precio.gasolina.publico?_id=587fbd68edfe99480a072f15").willReturn(aResponse()
                .withHeader("Content-Type", "application/json")
                .withBody(FileUtils.readAsText("mocks/api/responses/gasolineApiByIdResponse.json"))));

        GasolineDto gasolineDto = new ObjectMapper().readValue(FileUtils.readAsText("mocks/api/responses/gasolineResponse.json"), GasolineDto.class);
        GasOrder gasOrder = GasOrder.from(new ObjectMapper().readValue(FileUtils.readAsText("mocks/api/responses/infoRQRequest.json"), InfoRQ.class));

        gasOrder = gasService.createAndPersistGasOrder(gasOrder);

        Assertions.assertEquals(gasOrder.getFinalPrice().setScale(2, RoundingMode.HALF_DOWN).toString(), "5514.21");
        Assertions.assertEquals(gasOrder.getGasPrice().setScale(2, RoundingMode.HALF_DOWN).toString(), "15.71");


        gasService.findById(gasOrder.getId()).ifPresentOrElse(x -> {
            Assertions.assertEquals(x.getFinalPrice().setScale(2, RoundingMode.HALF_DOWN).toString(), "5514.21");
            Assertions.assertEquals(x.getGasPrice().setScale(2, RoundingMode.HALF_DOWN).toString(), "15.71");
        }, () -> {
            throw new RuntimeException("falló");
        });

        List<Client> a = clientsRepository.findAll();
        Assertions.assertEquals(1L, clientsRepository.findAll().size());
    }

    @Test
    public void shouldCreateTwoOrdersForExistingClient() throws IOException {

        String id = "587fbd68edfe99480a072f15";

        Client client = new Client("info@srpago.com","Juan","Perez");
        client = clientsRepository.save(client);

        Assertions.assertEquals("info@srpago.com", client.getEmail());
        Assertions.assertEquals(1, clientsRepository.findAll().size());

        wireMockRule.stubFor(WireMock.get("/v1/precio.gasolina.publico?_id=587fbd68edfe99480a072f15").willReturn(aResponse()
                .withHeader("Content-Type", "application/json")
                .withBody(FileUtils.readAsText("mocks/api/responses/gasolineApiByIdResponse.json"))));

        GasolineDto gasolineDto = new ObjectMapper().readValue(FileUtils.readAsText("mocks/api/responses/gasolineResponse.json"), GasolineDto.class);
        GasOrder gasOrder1 = GasOrder.from(new ObjectMapper().readValue(FileUtils.readAsText("mocks/api/responses/infoRQRequest.json"), InfoRQ.class));

        gasOrder1 = gasService.createAndPersistGasOrder(gasOrder1);

        Assertions.assertEquals(gasOrder1.getFinalPrice().setScale(2, RoundingMode.HALF_DOWN).toString(), "5514.21");
        Assertions.assertEquals(gasOrder1.getGasPrice().setScale(2, RoundingMode.HALF_DOWN).toString(), "15.71");


        gasService.findById(gasOrder1.getId()).ifPresentOrElse(x -> {
            Assertions.assertEquals(x.getFinalPrice().setScale(2, RoundingMode.HALF_DOWN).toString(), "5514.21");
            Assertions.assertEquals(x.getGasPrice().setScale(2, RoundingMode.HALF_DOWN).toString(), "15.71");
        }, () -> {
            Assertions.assertEquals(1, 2);
        });

        GasOrder gasOrder2 = GasOrder.from(new ObjectMapper().readValue(FileUtils.readAsText("mocks/api/responses/infoRQRequest.json"), InfoRQ.class));

        gasOrder2 = gasService.createAndPersistGasOrder(gasOrder2);

        Assertions.assertEquals(gasOrder2.getFinalPrice().setScale(2, RoundingMode.HALF_DOWN).toString(), "5514.21");
        Assertions.assertEquals(gasOrder2.getGasPrice().setScale(2, RoundingMode.HALF_DOWN).toString(), "15.71");


        gasService.findById(gasOrder2.getId()).ifPresentOrElse(x -> {
            Assertions.assertEquals(x.getFinalPrice().setScale(2, RoundingMode.HALF_DOWN).toString(), "5514.21");
        }, () -> {
            throw new RuntimeException("falló");
        });

        Assertions.assertEquals(1L, clientsRepository.findAll().size());
        Assertions.assertEquals(2L, ordersRepository.findAll().size());
    }

}
