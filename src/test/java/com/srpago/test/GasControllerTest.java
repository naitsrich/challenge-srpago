package com.srpago.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.srpago.controller.GasController;
import com.srpago.model.GasOrder;
import com.srpago.model.Response;
import com.srpago.model.dto.in.InfoRQ;
import com.srpago.service.impl.GasService;
import com.srpago.test.utils.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.io.IOException;

import static org.mockito.Mockito.doReturn;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class GasControllerTest {

    @InjectMocks
    GasController gasController;

    @Mock
    GasService gasService;

    MockMvc mockMvc;


    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(gasController).build();
    }

    @Test
    public void test() throws IOException {

        InfoRQ gasOrder = new ObjectMapper().readValue(FileUtils.readAsText("mocks/api/responses/infoRQRequest.json"), InfoRQ.class);

        doReturn(new GasOrder()).when(gasService).createAndPersistGasOrder(any());

        ResponseEntity<Response> a = gasController.generateOrder(gasOrder);

        Assertions.assertEquals(a.getStatusCodeValue(), 200);
    }

    @Test
    public void test2() throws Exception {
        final String responseBodyExpected = "{\"success\":true,\"error\":null,\"message\":\"InformaciÃ³n correcta\"}";
        InfoRQ gasOrder = new ObjectMapper().readValue(FileUtils.readAsText("mocks/api/responses/infoRQRequest.json"), InfoRQ.class);

        doReturn(new GasOrder()).when(gasService).createAndPersistGasOrder(any());

        MvcResult mvcResult = this.mockMvc.perform(
                post("/gas/order")
                        .contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(gasOrder))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andReturn();

        String content = mvcResult.getResponse().getContentAsString();
        Assertions.assertEquals(content, responseBodyExpected);
    }

}
