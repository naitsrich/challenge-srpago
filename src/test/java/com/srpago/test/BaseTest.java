package com.srpago.test;

import com.fasterxml.jackson.databind.ser.Serializers;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.srpago.client.impl.GasolineClient;
import com.srpago.service.IGasService;
import com.srpago.service.impl.GasService;
import org.junit.Rule;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.sql.DataSource;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;


@ExtendWith(SpringExtension.class)
@ContextConfiguration("file:src/test/resources/app-config.xml")
@TestPropertySource("classpath:application.properties")
@WebAppConfiguration
public class BaseTest {

    @Rule
    public static WireMockRule wireMockRule = new WireMockRule(wireMockConfig().port(8888));

    @BeforeAll
    public static void initWiremock() {
        wireMockRule.start();
    }

    @BeforeEach
    public void resetWiremock() {
        wireMockRule.resetAll();
    }

    @AfterAll
    public static void stopWiremock() {
        wireMockRule.stop();
    }


}
