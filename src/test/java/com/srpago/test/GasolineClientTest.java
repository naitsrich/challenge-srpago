package com.srpago.test;

import com.github.tomakehurst.wiremock.client.WireMock;
import static com.github.tomakehurst.wiremock.client.WireMock.*;
import com.srpago.client.IGasolineClient;
import com.srpago.exception.EntityNotFoundException;
import com.srpago.model.dto.in.GasolineDto;
import com.srpago.repository.OrdersRepository;
import com.srpago.test.utils.FileUtils;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

public class GasolineClientTest extends BaseTest {

    @Autowired
    OrdersRepository ordersRepository;

    @Autowired
    private IGasolineClient gasolineClient;

    @Test
    @DisplayName("should make request and get valid response")
    public void getValidResponse() throws IOException {
        String id = "587fbd68edfe99480a072f14";

        wireMockRule.stubFor(WireMock.get("/v1/precio.gasolina.publico?_id=587fbd68edfe99480a072f14").willReturn(aResponse()
                .withHeader("Content-Type", "application/json")
                .withBody(FileUtils.readAsText("mocks/api/responses/gasolineApiByIdResponse.json"))));

        GasolineDto gasolineDto = gasolineClient.findById(id);

       Assertions.assertEquals("587fbd68edfe99480a072f14", gasolineDto.get_id());
    }

    @Test
    @DisplayName("should make request and get valid response")
    public void getInvalidResponseStatusCode500() throws IOException {
        String id = "1";

        wireMockRule.stubFor(WireMock.get("/v1/precio.gasolina.publico?_id=1").willReturn(aResponse()
                .withHeader("Content-Type", "application/json")
                .withStatus(500)));

        Assertions.assertThrows(EntityNotFoundException.class, () -> {gasolineClient.findById(id);});
    }
}
