# challenge-srpago

Challenge Sr Pago

## What is this?

This is a Challenge Test for Sr Pago. The system simulate a gasoline purchase system.
Given an Order request, it goes to an external api to get info about Prices and generate a new "GasOrder" with the result of the purchase.

## How to run and deploy it?
Prerequisites:
- Tomcat installed
- MySQL installed in localhost:3306 and a Database "srpago" created with username "srpago" and password "1234". You can change this in the file main/resources/app-config.xml.
- Maven and Java 11
```
- [ ] Clone repo
- [ ] in root folder, run: mvn clean package
- [ ] Copy the generated war "challenge.war" in {root_folder}/target to {root_tomcat}/webapps
- [ ] Open {your_tomcat_location}/challenge/ in your browser. Try  {your_tomcat_location}/challenge/gas/order/1 to test if it is working
- [ ] Check your database. Liquibase creates automatically all the tables for you.
```
If you dont have MySql installed use the Docker Version instead inside docker folder and run it with:
```
- docker-compose up -d 
```


## Endpoints
- [ ] POST /gas/order --> Body IinfoRQ

Example:

```
{
"email" : "info@srpago.com",
"name" : "Juan",
"lastName" : "Perez",
"cardNumber" : "4242424242424242",
"expirationDateYear" : 2020,
"expirationDateMonth" : 12,
"gasType" :1,
"amount" : 350.00,
"gasStation" : “587fbd68edfe99480a072f15”,
"sellerName" : "Pedro Perez"
}
```

- [ ] GET /gas/order/{id}

To Improve: SWAGGER!

## Stack
- [ ] Java 
- [ ] Spring Mvc (No Boot)
- [ ] MySql
- [ ] Liquibase